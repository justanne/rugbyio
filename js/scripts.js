$(document).ready(function(){
	//Dropdown Nav
	$(".sub-nav").parent().append("<span></span>");

	$('.main-nav li span').mouseover(function() {
		$(this).parent().find(".sub-nav").slideDown('fast').show();
		$(this).parent().hover(function() {
		}, function(){
			$(this).parent().find(".sub-nav").slideUp('fast');
		});
	});
	
	//Header Borders
	$('h2').each(function() {
		var heading_width = $(this).width();
		$(this).wrapInner('<span class="heading" />');
		
		var text_width = $(this).find('.heading').width()+24;
		var border_width = Math.round((heading_width-text_width)/2);
		$(this).append('<span class="hr-left"></span><span class="hr-right"></span>');
		$(this).find('.hr-left, .hr-right').animate({'width': border_width});
	});
	
	$('h3').each(function() {
		var heading_width = $(this).width();
		$(this).wrapInner('<span class="heading" />');
		
		var text_width = $(this).find('.heading').width();
		var border_width = Math.round((heading_width-text_width));
		$(this).append('<span class="hr-right"></span>');
		$(this).find('.hr-right').animate({'width': border_width});
	});	

	//Input Clear
	swapValue = [];
	 $("input, textarea").each(function(i){
	 swapValue[i] = $(this).val();	

	 $(this).focus(function(){

	   if ($(this).val() == swapValue[i]) {
		 $(this).val("").css({'color':'#515d61'});
	    }
	   $(this).addClass("focus");
	 }).blur(function(){
	   if ($.trim($(this).val()) == "") {
	       $(this).val(swapValue[i]);
	       $(this).removeClass("focus").fadeIn('slow').css({'color':'#8e99a4'});
            }
	 });	 
     });

     //Popups
     $('#register').click(function(e) {
     $('#popup-register').lightbox_me('close');
    e.preventDefault();
	});

	 $('#login, #login-top').click(function(e) {
     $('#popup-login').lightbox_me('close');
    e.preventDefault();
	});

	//Back to Top Button
	$("#scroll-top").hide();
	
		//fade in
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#scroll-top').fadeIn();
			} else {
				$('#scroll-top').fadeOut();
			}
		});

		//scroll body to 0px on click
		$('#scroll-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});

	//Error Validate Form
	$(function() {
	
    $('.click-register').click(function() {
      	
      	$(function() {
      	//username      
      	var name = $("input#un").val();
  		if (name == "" | name == "Username") {
			$('input#un').after('<div class="error errtype1">Required. 30 characters or fewer. Letters, digits and  @/./+/-/_ only. <div class="point-left"></div></div>');  			
	        return false;
      	} else {$('.errtype1').fadeOut();}
      	});

      	$(function(){ 
    	//email 
      	var email = $("input#em").val();
  		if (email == "" | email == "Email") {  		
	  		$('input#em').after('<div class="error errtype2">Required. Must be in correct format: email@domain.com <div class="point-left"></div></div>');
	        return false;
      	} else {$('.errtype2').fadeOut();}
      	});

      	$(function(){ 
    	//password
      	var pass = $("input#pass").val();
  		if (pass == "" | pass == "Password") {      
	        $('input#pass-2').after('<div class="error errtype3">Oops. You wouldn\'t want to proceed without this. <div class="point-top"></div></div>');
	        $('.pull-top-20').css({'marginTop':'50px'});
	        return false;
      	} else {
	      	$('.errtype3').fadeOut();
	      	$('.pull-top-20').css({'marginTop':'20px'});}
      	});

      	$(function(){ 
    	//confirm password
      	var pass2 = $("input#pass-2").val();
  		if (pass2 == "" | pass2 == "Confirm Password") {        
	        $('input#pass-2').after('<div class="error errtype4">Type again the password to confirm. <div class="point-top"></div></div>');
	        $('.pull-top-20').css({'marginTop':'50px'});
	        return false;
      	} else {
	      	$('.errtype4').fadeOut();
	      	$('.pull-top-20').css({'marginTop':'20px'});}
      	});
    });


    $('.click-login').click(function() {
      	
      	$(function() {
      	//username login      
      	var name = $("input#un-log").val();
  		if (name == "" | name == "Username") {
			$('input#un-log').after('<div class="error errtype5">Enter your Username.<div class="point-top"></div></div>');  			
			$('input#pass-log').css({'marginTop':'50px'});
	        return false;
      	} else {
	      	$('.errtype5').fadeOut(function(){
	      	$('input#pass-log').css({'marginTop':'0px'});
	      	});
      		}
      	});

      	$(function(){ 
    	//password login 
      	var passlog = $("input#pass-log").val();
  		if (passlog == "" | passlog == "Password") {  		
	  		$('input#pass-log').after('<div class="error errtype6">Don\'t forget your Password.<div class="point-top"></div></div>');
	  		$('.pull-top-20 .click-login').css({'marginTop':'50px'});
	        return false;
      	} else {
	      	$('.errtype6').fadeOut(function(){
	      	$('.pull-top-20 .click-login').css({'marginTop':'0px'});
	      	});      		
      		}
      	});
    });


    $('.click-sendmsg').click(function() {
      	var hasError = false; //setting value as false
      	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

      	$(function() {
      	//name      
      	var cname = $("input#contact-name").val();
  		if (cname == "" | cname == "Your Name") {
			$('input#contact-name').after('<div class="error errtype5">What is your name? <div class="point-top"></div></div>');  			
			$('.errtype5').css({'width':'220px'});
			$('#contact-subj').css({'marginTop':'50px'});
	        hasError = true;
      	} else {
	      	$('.errtype5').fadeOut(function(){
      		$('#contact-subj').css({'marginTop':'0px'});
      		});
      		}    
      	});

      	$(function(){ 
    	//subject 
      	var subj = $("input#contact-subj").val();
  		if (subj == "" | subj == "Subject") {  		
	  		$('input#contact-subj').after('<div class="error errtype6">Tell me, what is it about? <div class="point-top"></div></div>');
	  		$('.errtype6').css({'width':'220px'});
			$('#contact-mail').css({'marginTop':'50px'});
	        hasError = true;
      	} else {
	      	$('.errtype6').fadeOut(function(){
      		$('#contact-mail').css({'marginTop':'0px'});
      		});
      		}
      	});

      	$(function(){ 
    	//email address
      	var cmail = $("input#contact-mail").val();
  		if (cmail == "" | cmail == "Your email address") {      
	        $('input#contact-mail').after('<div class="error errtype7">Please provide your email address. No spams, promise. <div class="point-top"></div></div>');	     
	        $('.errtype7').css({'width':'220px'});	     
			hasError = true;	        
      	} else if(!emailReg.test(emailVal)) {
      		$('input#contact-mail').after('<div class="error errtype7">The email address you entered is not valid.<div class="point-top"></div></div>');	     
	        $('.errtype7').css({'width':'220px'});
	        hasError = true;
      	}
      	  else {$('.errtype7').fadeOut();}
      	});

      	$(function(){ 
    	//message content
      	var msg = $("textarea#contact-msg").val();
  		if (msg == "" | msg == "Message") {        
	        $('textarea#contact-msg').after('<div class="error errtype8">You forgot this! <div class="point-top"></div></div>');	       
	        hasError = true;
      	} else {$('.errtype8').fadeOut();}
      	});

      	if(hasError == false) {
      		$.post("sendmail.php", {
			fullname: cname, email: cmail, subject: subj, message: msg
      		});
      	}
    });

  	});
  

});
